<?php

/**
-     * @author Aamir Ali Mir
-     * @date 24Jan2018
-     *   
**/
require_once "PlobalMailer/classes/GetReCaptcha.php";
require_once "PlobalMailer/classes/PutMail.php";
require_once "PlobalMailer/config.php";

$data =  [
	      'tos' => $tos,
	      'subject' => $subject,
	      'send_from' => ['email'=> $send_from[0], 'name'=> $send_from[1]]
	     ];
$attribute = [
              'credentials' => ['email'=> $credentials_email, 'password' => $credentials_password]
             ];

if($_POST){

	$post_data = $_POST;

	if($has_recaptcha){

		if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){

		    $site_key = $_POST["g-recaptcha-response"];

		    $reCaptcha = new GetReCaptcha($secret, $site_key);

		    if($reCaptcha->response){

		    	if(sendMail($attribute, $data)){

		    		header("Location:". $thanks_page."?status=thanks");
		    		exit;

		    	}

		    }else{
		    	//You are a bot! Go away!
		    	header('Location: ' . $_SERVER['HTTP_REFERER'].'?status=You+are+a+bot!+Go+away!');
			    exit;
		    }
		}else{
			//Please click on the reCAPTCHA box
			header('Location: ' . $_SERVER['HTTP_REFERER'].'?status=Please+click+on+the+reCAPTCHA+box');
			exit;
		}

	}else{

		$get_response = makeBody($body_fields_required, $body_fields_optional, $post_data);

		if($get_response['has_error']){

			//send error $get_response['error_msg'];
			header("Location:". $error_page."?status=".$get_response['error_msg']);
			exit;
		}

		$data['body'] = $get_response['body'];

		if(sendMail($attribute, $data)){

			header("Location: thank-you.html?status=thanks");
		    exit;

		}
	}

}

function sendMail($attribute, $data){

	$mail = new PutMail($attribute, $data);

	return $mail->response();
}
function makeBody($body_fields_required, $body_fields_optional, $post_data){

	$body = '';
	$has_error = false;
	$error_msg = '';

	foreach ($body_fields_required as $key) {

		if (array_key_exists($key, $post_data)) {
			$body_field = "";
			$body_field .= ucwords($key).": ";
			$body_field .= $post_data[$key];
			$body_field .= "<br/>";

			$body = $body . $body_field;

		} else {
			$has_error = true;
			$error_msg = $key.' field is required';
			break;
		}
		
	}
	if(!$has_error){
		foreach ($body_fields_optional as $key) {
			if (array_key_exists($key, $post_data)) {

				$body_field = "";
				$body_field .= ucwords($key).": ";
				$body_field .= $post_data[$key];
				$body_field .= "<br/>";

				$body = $body . $body_field;

			}
		}
	}

	return ['body'=>$body, 'has_error'=>$has_error, 'error_msg'=> $error_msg];
}

?>