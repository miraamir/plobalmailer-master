<?php
/**
-     * @author Aamir Ali Mir
-     * @date 24Jan2018
-     *   
**/

require_once "PlobalMailer/includes/recaptcha-1.0.0/php/recaptchalib.php";

class GetReCaptcha
{
    private $_response = null;
    private $_site_key, $_secret;

    public function __construct($secret, $site_key) {

        $this->_secret = $secret;
        $this->_site_key = $site_key;
    }

    private function verifyReCaptcha($value ) {

        $reCaptcha = new ReCaptcha($_secret);

        $this->_response = $reCaptcha->verifyResponse( $_SERVER["REMOTE_ADDR"], $this->_site_key );
    }

    public function response() {

        return $this->_response->success;
    }

}