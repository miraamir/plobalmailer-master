<?php
/**
-     * @author Aamir Ali Mir
-     * @date 24Jan2018
-     *   
**/
//require_once 'PlobalMailer/includes/PHPMailer-master/PHPMailerAutoload.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


require_once 'PlobalMailer/includes/PHPMailer-master/src/Exception.php';
require_once 'PlobalMailer/includes/PHPMailer-master/src/PHPMailer.php';
require_once 'PlobalMailer/includes/PHPMailer-master/src/SMTP.php';

class PutMail
{
    private $_response = null;
    private $_tos, $_subject, $_send_from, $_body, $_credentials;
    private $_mail;

    public function __construct($attribute = [], $data = []) {

          

        $this->_tos = $data['tos'];
        $this->_body = $data['body'];
        $this->_send_from = $data['send_from'];
        $this->_subject = $data['subject'];
        $this->_credentials = $attribute['credentials'];

        $this->_mail = new PHPMailer;

        $this->sendMail();
    }

    private function sendMail() {  

        try {     

            $this->setMailer();
            $this->setAddress();
            $this->setBody();        

            $this->_mail->send();
            $this->_response = 'Message has been sent';


        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
            exit;
        }
    }

    private function setMailer() {

        $this->_mail->isSMTP();

        $this->_mail->SMTPDebug = 0; //2
        $this->_mail->Debugoutput = 'html';
        $this->_mail->Host = 'smtp.gmail.com';        
        $this->_mail->Port = 587;
        $this->_mail->SMTPSecure = 'tls';
        $this->_mail->SMTPAuth = true;

        $this->_mail->Username = $this->_credentials['email'];        
        $this->_mail->Password = $this->_credentials['password'];
    }

    private function setAddress() {

        $this->_mail->setFrom($this->_send_from['email'], $this->_send_from['name']);
        $this->_mail->Subject = $this->_subject;

        foreach($this->_tos as $to ) {
            $this->_mail->addAddress($to); 
        }
    }
    private function setBody() {

        $this->_mail->msgHTML($this->_body);
    }

    public function response() {

        return $this->_response;
    }
}

?>